package cat.itb.testingconcepts;

import android.view.View;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest
{
    private Matcher<View>
            title = ViewMatchers.withId(R.id.title_main),
            buttonMain = ViewMatchers.withId(R.id.button_main),
            buttonSecond = ViewMatchers.withId(R.id.button_second),
            inputUsername = ViewMatchers.withId(R.id.username_input),
            inputPassword = ViewMatchers.withId(R.id.password_input);

    private static final String TITLE_TEXT = "Main Activity Title";
    private static final String NEXT_TEXT = "Next";
    private static final String BACK_TEXT = "Back";

    private static final String LOGGED_TEXT = "Logged";

    private static final String USER_TO_BE_TYPED = "Willyrex77";
    private static final String PASS_TO_BE_TYPED = "mellamojorge";

    @Rule
    public ActivityScenarioRule<MainActivity> mainActivityActivityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void title_and_button_displayed()
    {
        Espresso.onView(title).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(buttonMain).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
    }

    @Test
    public void title_text_correct_and_button_text_correct()
    {
        Espresso.onView(title).check(ViewAssertions.matches(ViewMatchers.withText(TITLE_TEXT)));
        Espresso.onView(buttonMain).check(ViewAssertions.matches(ViewMatchers.withText(NEXT_TEXT)));
    }

    @Test
    public void next_button_clickable_and_changes_text_to_back()
    {
        Espresso.onView(buttonMain)
                .check(ViewAssertions.matches(ViewMatchers.isClickable()))
                .perform(ViewActions.click())
                .check(ViewAssertions.matches(ViewMatchers.withText(BACK_TEXT)));
    }

    @Test
    public void login_form_behaviour()
    {
        writeInput();
        Espresso.onView(buttonMain)
                .perform(ViewActions.click())
                .check(ViewAssertions.matches(ViewMatchers.withText(LOGGED_TEXT)));
    }

    private void writeInput()
    {
        Espresso.onView(inputUsername)
                .perform(ViewActions.typeText(USER_TO_BE_TYPED), ViewActions.closeSoftKeyboard())
                .check(ViewAssertions.matches(ViewMatchers.withText(USER_TO_BE_TYPED)));
        Espresso.onView(inputPassword)
                .perform(ViewActions.typeText(PASS_TO_BE_TYPED), ViewActions.closeSoftKeyboard())
                .check(ViewAssertions.matches(ViewMatchers.withText(PASS_TO_BE_TYPED)));
    }

    @Test
    public void go_to_second_activity()
    {
        Espresso.onView(buttonMain).perform(ViewActions.click()).check(ViewAssertions.doesNotExist());
    }

    @Test
    public void go_from_second_activity_to_main_activity()
    {
        go_to_second_activity();
        Espresso.onView(buttonSecond).perform(ViewActions.click());
        title_and_button_displayed();

        go_to_second_activity();
        Espresso.pressBack();
        title_and_button_displayed();
    }

    @Test
    public void check_login_buttonNext_change_activity_secondTitle_backButton_fromSecondToMain_loginIsEmpty()
    {
        writeInput();
        go_to_second_activity();
        Espresso.onView(ViewMatchers.withId(R.id.title_second))
                .check(ViewAssertions.matches(ViewMatchers.withText("Welcome back " + USER_TO_BE_TYPED)));

        Espresso.onView(buttonSecond).perform(ViewActions.click());
        title_and_button_displayed();

        Espresso.onView(inputUsername).check(ViewAssertions.matches(ViewMatchers.withText("")));
        Espresso.onView(inputPassword).check(ViewAssertions.matches(ViewMatchers.withText("")));
    }
}
