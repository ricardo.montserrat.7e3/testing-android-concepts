package cat.itb.testingconcepts;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Button secondButton = findViewById(R.id.button_second);

        TextView secondTitle = findViewById(R.id.title_second);

        Bundle values = getIntent().getExtras();

        if(values != null)
        {
            secondTitle.setText(String.format("Welcome back %s", values.getString("username")));
        }

        secondButton.setOnClickListener(x -> startActivity(new Intent(SecondActivity.this, MainActivity.class)));
    }
}