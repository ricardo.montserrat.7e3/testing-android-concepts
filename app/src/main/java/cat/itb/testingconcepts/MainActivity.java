package cat.itb.testingconcepts;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
{
    private static final String USER_TO_BE_TYPED = "Willyrex77";
    private static final String PASS_TO_BE_TYPED = "mellamojorge";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView title = findViewById(R.id.title_main);
        final Button button = findViewById(R.id.button_main);

        final EditText userInput = findViewById(R.id.username_input);
        final EditText passwordInput = findViewById(R.id.password_input);

        button.setOnClickListener(x ->
        {
            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
            intent.putExtra("username", userInput.getText().toString());
            startActivity(intent);
        });
    }
}